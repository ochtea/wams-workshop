package com.example.test_todo0001;

import static com.microsoft.windowsazure.mobileservices.MobileServiceQueryOperations.*;

import java.net.MalformedURLException;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.google.android.gcm.GCMRegistrar;
import com.microsoft.windowsazure.mobileservices.MobileServiceAuthenticationProvider;
import com.microsoft.windowsazure.mobileservices.MobileServiceClient;
import com.microsoft.windowsazure.mobileservices.MobileServiceTable;
import com.microsoft.windowsazure.mobileservices.MobileServiceUser;
import com.microsoft.windowsazure.mobileservices.NextServiceFilterCallback;
import com.microsoft.windowsazure.mobileservices.ServiceFilter;
import com.microsoft.windowsazure.mobileservices.ServiceFilterRequest;
import com.microsoft.windowsazure.mobileservices.ServiceFilterResponse;
import com.microsoft.windowsazure.mobileservices.ServiceFilterResponseCallback;
import com.microsoft.windowsazure.mobileservices.TableOperationCallback;
import com.microsoft.windowsazure.mobileservices.TableQueryCallback;
import com.microsoft.windowsazure.mobileservices.UserAuthenticationCallback;

public class ToDoActivity extends Activity {

    private static final String TAG = "ToDoActivity";

    /**
     * Mobile Service Client reference
     */
    private MobileServiceClient mClient;

    /**
     * Mobile Service Table used to access data
     */
    private MobileServiceTable<ToDoItem> mToDoTable;

    /**
     * Adapter to sync the items list with the view
     */
    private ToDoItemAdapter mAdapter;

    /**
     * EditText containing the "New ToDo" text
     */
    private EditText mTextNewToDo;

    /**
     * Progress spinner to use for table operations
     */
    private ProgressBar mProgressBar;

    /**
     * GCM Registration ID
     */
    private String mRegistationId;

    /**
     * Initializes the activity
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do);

        // Initialize the progress bar
        mProgressBar = (ProgressBar) findViewById(R.id.loadingProgressBar);
        mProgressBar.setVisibility(ProgressBar.GONE);

        // Check and register GCM
        GCMRegistrar.checkDevice(this);
        GCMRegistrar.checkManifest(this);
        mRegistationId = GCMRegistrar.getRegistrationId(this);
        Log.i(TAG, "RegistrationID is : " + mRegistationId);
        if (TextUtils.isEmpty(mRegistationId)) {
            GCMRegistrar.register(this, ApplicationConst.GCM_SENDER_ID);
            mRegistationId = GCMIntentService.getRegistrationId();
        }

        try {
            // Create the Mobile Service Client instance, using the provided
            // Mobile Service URL and key
            mClient = new MobileServiceClient(
                    ApplicationConst.MOBILE_SERVICE_URL,
                    ApplicationConst.MOBILE_SERVICE_APP_KEY, this)
                    .withFilter(new ProgressFilter());
        } catch (MalformedURLException e) {
            createAndShowDialog(
                    new Exception(
                            "There was an error creating the Mobile Service. Verify the URL"),
                    "Error");
        }

        MobileServiceUser user = getStoredMobileServiceUser();
        if (user == null) {
            showProviderSelectDialog();
        } else {
            createToDoTableView();
        }
    }

    /**
     * Initializes the activity menu
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    /**
     * Select an option from the menu
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_refresh) {
            refreshItemsFromTable();
        } else if (item.getItemId() == R.id.menu_logout) {
            mClient.logout();
            clearStoredMobileServiceUserData();
            showProviderSelectDialog();
        }
        return true;
    }

    /**
     * Mark an item as completed
     * 
     * @param item
     *            The item to mark
     */
    public void checkItem(ToDoItem item) {
        if (mClient == null) {
            return;
        }

        // Set the item as completed and update it in the table
        item.setComplete(true);

        mToDoTable.update(item, new TableOperationCallback<ToDoItem>() {
            public void onCompleted(ToDoItem entity, Exception exception,
                    ServiceFilterResponse response) {
                if (exception == null) {
                    if (entity.isComplete()) {
                        mAdapter.remove(entity);
                    }
                } else {
                    createAndShowDialog(exception, "Error");
                }
            }
        });
    }

    /**
     * Add a new item
     * 
     * @param view
     *            The view that originated the call
     */
    public void addItem(View view) {
        if (mClient == null) {
            return;
        }

        // Create a new item
        ToDoItem item = new ToDoItem();

        item.setText(mTextNewToDo.getText().toString());
        item.setRegistrationId(TextUtils.isEmpty(mRegistationId) ? ""
                : mRegistationId);
        item.setComplete(false);

        // Insert the new item
        mToDoTable.insert(item, new TableOperationCallback<ToDoItem>() {
            public void onCompleted(ToDoItem entity, Exception exception,
                    ServiceFilterResponse response) {
                if (exception == null) {
                    if (!entity.isComplete()) {
                        mAdapter.add(entity);
                    }
                } else {
                    createAndShowDialog(exception, "Error");
                }
            }
        });
        mTextNewToDo.setText("");
    }

    /**
     * Refresh the list with the items in the Mobile Service Table
     */
    private void refreshItemsFromTable() {
        // Get the items that weren't marked as completed and add them in the
        // adapter
        mToDoTable.where().field("complete").eq(val(false))
                .execute(new TableQueryCallback<ToDoItem>() {
                    public void onCompleted(List<ToDoItem> result, int count,
                            Exception exception, ServiceFilterResponse response) {
                        if (exception == null) {
                            mAdapter.clear();
                            for (ToDoItem item : result) {
                                mAdapter.add(item);
                            }
                        } else {
                            createAndShowDialog(exception, "Error");
                        }
                    }
                });
    }

    /**
     * Creates a dialog and shows it
     * 
     * @param exception
     *            The exception to show in the dialog
     * @param title
     *            The dialog title
     */
    private void createAndShowDialog(Exception exception, String title) {
        Throwable ex = exception;
        if (exception.getCause() != null) {
            ex = exception.getCause();
        }
        createAndShowDialog(ex.getMessage(), title);
    }

    /**
     * Creates a dialog and shows it
     * 
     * @param message
     *            The dialog message
     * @param title
     *            The dialog title
     */
    private void createAndShowDialog(String message, String title) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setPositiveButton("OK", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private class ProgressFilter implements ServiceFilter {
        @Override
        public void handleRequest(ServiceFilterRequest request,
                NextServiceFilterCallback nextServiceFilterCallback,
                final ServiceFilterResponseCallback responseCallback) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mProgressBar != null)
                        mProgressBar.setVisibility(ProgressBar.VISIBLE);
                }
            });

            nextServiceFilterCallback.onNext(request,
                    new ServiceFilterResponseCallback() {
                        @Override
                        public void onResponse(ServiceFilterResponse response,
                                Exception exception) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (mProgressBar != null)
                                        mProgressBar
                                                .setVisibility(ProgressBar.GONE);
                                }
                            });
                            if (responseCallback != null) {
                                responseCallback
                                        .onResponse(response, exception);
                            }
                        }
                    });
        }
    }

    private void authenticateUser(MobileServiceAuthenticationProvider provider) {
        // Login using the Twitter provider.
        UserAuthenticationCallback callback = new UserAuthenticationCallback() {
            @Override
            public void onCompleted(MobileServiceUser user,
                    Exception exception, ServiceFilterResponse response) {
                if (exception == null) {
                    createAndShowDialog(
                            String.format("You are now logged in - %1$2s",
                                    user.getUserId()), "Success");
                    // Store user data to shared preference
                    storeMobileServiceUserData();

                    createToDoTableView();
                } else {
                    createAndShowDialog("You must log in. Login Required",
                            "Error");
                }
            }
        };
        mClient.login(provider, callback);
    }

    private void createToDoTableView() {
        // Get the Mobile Service Table instance to use
        mToDoTable = mClient.getTable(ToDoItem.class);
        mTextNewToDo = (EditText) findViewById(R.id.textNewToDo);

        // Create an adapter to bind the items with the view
        mAdapter = new ToDoItemAdapter(this, R.layout.row_list_to_do);
        ListView listViewToDo = (ListView) findViewById(R.id.listViewToDo);
        listViewToDo.setAdapter(mAdapter);

        // Load the items from the Mobile Service
        refreshItemsFromTable();
    }

    private void storeMobileServiceUserData() {
        SharedPreferences settings = this.getSharedPreferences("UserData",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        preferencesEditor.putString("userid", mClient.getCurrentUser()
                .getUserId());
        preferencesEditor.putString("token", mClient.getCurrentUser()
                .getAuthenticationToken());
        preferencesEditor.commit();
    }

    private void clearStoredMobileServiceUserData() {
        SharedPreferences settings = this.getSharedPreferences("UserData",
                Context.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = settings.edit();
        preferencesEditor.clear().commit();
    }

    private MobileServiceUser getStoredMobileServiceUser() {
        SharedPreferences settings = this.getSharedPreferences("UserData",
                Context.MODE_PRIVATE);

        String userID = settings.getString("userid", null);
        String token = settings.getString("token", null);

        if (TextUtils.isEmpty(userID) || TextUtils.isEmpty(token)) {
            return null;
        }
        MobileServiceUser user = new MobileServiceUser(userID);
        user.setAuthenticationToken(token);
        mClient.setCurrentUser(user);
        return user;
    }

    private void showProviderSelectDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        final String items[] = new String[] { "Facebook", "Twitter", "Google",
                "MicrosoftAccount" };
        dialog.setSingleChoiceItems(items, 0, new OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int n) {
                authenticateUser(MobileServiceAuthenticationProvider
                        .valueOf(items[n]));
                d.dismiss();
            }
        });
        dialog.setTitle("Please select provider to authenticate");
        dialog.show();
    }
}
