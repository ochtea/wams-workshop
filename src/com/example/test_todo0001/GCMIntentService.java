package com.example.test_todo0001;

import android.content.Context;
import android.content.Intent;
import android.app.NotificationManager;
import android.support.v4.app.NotificationCompat;

import com.google.android.gcm.GCMBaseIntentService;

public class GCMIntentService extends GCMBaseIntentService {

    private static String sRegistrationId;

    public static String getRegistrationId() {
        return sRegistrationId;
    }

    public GCMIntentService() {
        super(ApplicationConst.GCM_SENDER_ID);
    }

    @Override
    protected void onError(Context arg0, String arg1) {
        // TODO Auto-generated method stub
    }

    @Override
    protected void onMessage(Context context, Intent intent) {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                this).setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("New ToDo item!")
                .setContentText(intent.getStringExtra("message"));
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }

    @Override
    protected void onRegistered(Context context, String registrationId) {
        sRegistrationId = registrationId;
    }

    @Override
    protected void onUnregistered(Context arg0, String arg1) {
        // TODO Auto-generated method stub
    }

}
